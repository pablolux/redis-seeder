.DEFAULT_GOAL := help
SHELL = /bin/sh

build: ## Builds the container(s)
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose build

start: ## Starts the container(s)
	docker-compose up

install: ## Starts installs node packages
	docker-compose run service npm install

tests: ## Runs integration tests
	docker-compose run service npm run test

tests-coverage: ## Runs integration tests with coverage
	docker-compose run service npm run test-coverage

lint: ## runs eslint
	docker-compose run service npm run lint

help: ## Show this help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

