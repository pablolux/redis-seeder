import logger from '@luxerone/file-logger';
import { createClient } from 'redis';
import {exit} from "process";

logger.info('Hello world!');



const p = async () => {
    const elements = [];
    elements.push(JSON.stringify({
        feedId: 123,
        type: "Type",
        name: "Name",
        processor: "email",
    }));

    const redisClient = createClient({
        url: process.env.REDIS_CONN_STRING,
    });
    try {
        await redisClient.connect();
        await redisClient.rPush(process.env.SYNC_RESIDENT_FEEDS_QUEUE || 'sync_resident_feeds', elements);
    } catch (e: unknown) {
        logger.info(e || 'Unexpected error when attempting connection to Redis');
        exit(1);
    }
};

p();