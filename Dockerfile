FROM node as base

ARG GITLAB_DEPLOY_TOKEN
RUN npm config set @luxerone:registry=https://gitlab.com/api/v4/projects/35328848/packages/npm/
RUN npm config set //gitlab.com/api/v4/projects/35328848/packages/npm/:_authToken=$GITLAB_DEPLOY_TOKEN

WORKDIR /app
COPY package*.json ./
COPY tsconfig.json ./

FROM base as production
ENV NODE_ENV=production
RUN npm ci
COPY . .
RUN npm run build
CMD npm run prod

FROM base as dev
ENV NODE_ENV=development
RUN chown -R node.node /tmp

EXPOSE 8080

